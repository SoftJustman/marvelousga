// ==UserScript==
// @name         Marvelous_extended
// @namespace    https://marvelousga.com/
// @version      1.3.0
// @description  Extending MarvelousGA functional
// @homepage     https://bitbucket.org/SoftJustman/marvelousga
// @icon         https://i.imgur.com/HddFKWQ.png
// @updateURL    https://bitbucket.org/SoftJustman/marvelousga/downloads/script.user.js
// @downloadURL  https://bitbucket.org/SoftJustman/marvelousga/downloads/script.user.js
// @supportURL   https://bitbucket.org/SoftJustman/marvelousga/issues
// @author       Justman
// @match        https://marvelousga.com/giveaway.php?id=*
// @match        https://marvelousga.com/dashboard.php
// @match        https://marvelousga.com/raffle.php?id=*
// @match        https://marvelousga.com/raffles.php
// @run-at       document-end
// @grant        GM_getValue
// @grant        GM_setValue
// @grant        GM_deleteValue
// @grant        GM_setClipboard
// @grant        GM_xmlhttpRequest
// @grant        GM_addStyle
// @grant        GM_registerMenuCommand
// @grant        GM_notification
// @grant        GM_info
// @connect      api.steampowered.com
// @connect      steamcommunity.com
// ==/UserScript==


(function() {
    "use strict";
/*
    Global variables
*/
    /*
        URL
    */
    var STEAM_GROUPS_PAGE_URL = "http://steamcommunity.com/my/groups";

    /*
        User data, settings
    */
    var IF_LOADED_ACTUAL_DATA = false; // becomes true when current user data loaded

    var USER =
    {
        ID: "", // SteamID64
        SESSION_ID: "", // Steam session ID
        PROCESS_URL: "", // URL for some requests
        VISITED_GA_LIST: {}, // Looks like pairs int id: bool ifGotReward
        VISITED_RAFFLE_LIST: {}, // Looks like pairs int id: bool ifGotReward
        GROUPS_ARRAY: [], // List of user group names

        load: function(callback)
        {
            console.log("    Loading user data");
            var lastUserId = GM_getValue("last_user_id"); // last user Steam64ID
            if (lastUserId) {
                var lastUserData = GM_getValue("user_" + lastUserId);
                if (lastUserData) {
                    lastUserData = JSON.parse(lastUserData);
                    console.log("    Using last user data: ", lastUserData);
                    this.ID = lastUserData.ID;
                    if (lastUserData.VISITED_GA_LIST) this.VISITED_GA_LIST = lastUserData.VISITED_GA_LIST;
                    if (lastUserData.VISITED_RAFFLE_LIST) this.VISITED_RAFFLE_LIST = lastUserData.VISITED_RAFFLE_LIST;
                    if (lastUserData.GROUPS_ARRAY) this.GROUPS_ARRAY = lastUserData.GROUPS_ARRAY;
                } else {
                    console.log("    Was not able to load last user data. Last user ID: ", lastUserId);
                }
            }
            var this_ = this;
            var listRequest =
            {
                "method" : "GET",
                "url" : STEAM_GROUPS_PAGE_URL,
                onload: function(listResponse) {
                    var str = listResponse.response;
                    var tempArr = str.match(/<a class="linkTitle" href="http:\/\/steamcommunity.com\/groups\/\S{1,}">/g);
                    if (tempArr) {
                        for (var i = 0; i < tempArr.length; i++) {
                            this_.GROUPS_ARRAY[i] = tempArr[i].slice(60, -2).toLowerCase();
                        }
                    } else {
                        this_.GROUPS_ARRAY = [];
                    }
                    this_.ID = str.match(/g_steamID = "\S{1,}"/)[0].slice(13, -1);
                    this_.SESSION_ID = str.match(/g_sessionID = "\S{1,}"/)[0].slice(15, -1);
                    this_.PROCESS_URL = str.match(/processURL = '\S{1,}'/)[0].slice(14, -1);
                    // Filled ID, sessionID, processURL and groups array

                    var memStr = GM_getValue("user_" + this_.ID);
                    if (memStr) {
                        var memData = JSON.parse(memStr);
                        if (memData.VISITED_GA_LIST) this_.VISITED_GA_LIST = memData.VISITED_GA_LIST;
                        if (memData.VISITED_RAFFLE_LIST) this_.VISITED_RAFFLE_LIST = memData.VISITED_RAFFLE_LIST;
                    }

                    console.log("    Loaded user data: ", this_);
                    IF_LOADED_ACTUAL_DATA = true;

                    this_.save();
                    callback();
                },
                onerror: function(error) {
                    alert("Unable to get Steam data. Try to refresh this page");
                }
            };
            GM_xmlhttpRequest(listRequest);
        },

        save: function()
        {
            if (!this.ID) {
                return;
            }
            var str = JSON.stringify(this);
            GM_setValue("user_" + this.ID, str);
            GM_setValue("last_user_id", this.ID);
            console.log("    Saved user data: ", this);
        }
    };

    /*
        Page location
    */
    var LOCATION = ""; // Describes current location
    if (document.URL.indexOf("giveaway.php") != -1) LOCATION += "G";
    if (document.URL.indexOf("raffle.php") != -1) LOCATION += "R";
    if (document.URL.indexOf("dashboard.php") != -1 || document.URL.indexOf("raffles.php") != -1) LOCATION += "D";

    /*
        Giveaway data
    */
    var GIVEAWAY =
    {
        ID: "",
        updateID: function()
        {
            this.ID = document.URL.match(/\?id=[0-9]{1,}/)[0].slice(4);
            console.log("    Updated GA ID: ", this.ID);
            return this.ID;
        },

        TASKS_ARRAY: [], // List of tasks
        updateTaskArray: function()
        {
            if (this.TASKS_ARRAY.length) {
                return;
            }
            var taskElements = Array.prototype.slice.call(document.getElementsByClassName("panel-body")[1].children);
            for (var i = 0; i < taskElements.length; i++) {
                if (taskElements[i].innerHTML.indexOf("<a class=\"btn btn-danger btn-xs\">") == -1) {
                    taskElements.splice(i, 1);
                    i--;
                }
            }
            for (var i = 0; i < taskElements.length; i++) {
                var tID = taskElements[i].children[0].children[1].getAttribute("data-target").substr(9);
                this.TASKS_ARRAY[i] =
                {
                    "element": taskElements[i],
                    "id": tID,
                    "buttonsContainer": document.getElementById("collapse" + tID),
                    "inner": taskElements[i].innerText.substr(1, taskElements[i].innerText.length - 3),
                    "steam": false // null or URL of group
                };
                if (this.TASKS_ARRAY[i]["inner"].match(REGEXP.STEAM_TASK)) {
                    var str = this.TASKS_ARRAY[i]["buttonsContainer"].getElementsByTagName("a")[0].getAttribute("href");
                    str = str.split("/")[4].replace(/[#]/, ""); // Some unexcepted symbols
                    str = str.toLowerCase(); // lower case

                    this.TASKS_ARRAY[i]["steam"] =
                    {
                        id: str,
                        ifJoined: false
                    };
                }
            }
            console.log("    Updated task array: ", this.TASKS_ARRAY);
            return this.TASKS_ARRAY;
        },

        updateSteamTasksJoinStatus: function ()
        {
            for (var i = 0; i < this.TASKS_ARRAY.length; i++) {
                if (this.TASKS_ARRAY[i]["steam"]) {
                    if (findInArray(USER.GROUPS_ARRAY, this.TASKS_ARRAY[i]["steam"]["id"]) == -1) {
                        this.TASKS_ARRAY[i]["steam"]["ifJoined"] = false;
                    } else {
                        this.TASKS_ARRAY[i]["steam"]["ifJoined"] = true;
                    }
                }
            }
        },

        highlight: function()
        {
            console.log("    Highlighting tasks");
            this.updateTaskArray();
            for (var i = 0; i < this.TASKS_ARRAY.length; i++) {
                console.log("    Task #" + i);
                var tElem = this.TASKS_ARRAY[i]["element"].children[0].children[0];
                if (this.TASKS_ARRAY[i]["steam"]) {
                    console.log("    Group URL: " + this.TASKS_ARRAY[i]["steam"]["id"]);
                    if (findInArray(USER.GROUPS_ARRAY, this.TASKS_ARRAY[i]["steam"]["id"]) != -1) {
                        setBoxShadow(tElem, "g");
                    } else {
                        if (IF_LOADED_ACTUAL_DATA) {
                            setBoxShadow(tElem, "r");
                        } else {
                            setBoxShadow(tElem, "y");
                        }
                    }
                } else if (this.TASKS_ARRAY[i]["inner"].indexOf("Sign up and earn free Steam Wallet codes!") != -1) {
                    setBoxShadow(tElem, "p");
                } else if (this.TASKS_ARRAY[i]["inner"].indexOf("Twitch") != -1) {
                    setBoxShadow(tElem, "p");
                } else {
                    setBoxShadow(tElem, "b");
                    /*
                        Here must remove links and click;
                        Actually, it's not necessary (yet) so eff that
                    */
                }
            }
            console.log("    Highlighted all tasks");
        },

        getKeyField: function()
        {
            var t = document.getElementsByClassName("panel-body")[1].children;
            for (var i = 0; i < t.length; i++) {
                if (t[i].innerHTML.indexOf("Your key") != -1) {
                    return t[i];
                }
            }
            console.log("    Unable to find key field or key is not recieved yet");
            return null;
        },

        ifGotReward: function()
        {
            this.updateTaskArray();
            var keyField = this.getKeyField();
            return keyField && keyField.innerHTML.indexOf("XXXX-XXXX-XXXX") == -1;
        },

        modifyKeyField: function()
        {
            if (this.ifGotReward()) {
                this.updateTaskArray();
                var keyField = this.getKeyField();
                var inner = keyField.children[1].innerText;
                if (inner.match(REGEXP.STEAM_KEY)) {
                    keyField.setAttribute("style", "cursor: pointer;");
                    keyField.addEventListener("click", function(event) {
                        GM_setClipboard(inner.match(REGEXP.STEAM_KEY)[0], "{ type: 'text', mimetype: 'text/plain'}");
                    });
                    console.log("    Key field is now copiable");
                } else if (inner.match(REGEXP.URL)) {
                    keyField.innerHTML = "<a href=\"" + inner.match(REGEXP.URL)[0] + "\" target=\"_blank\">" + keyField.innerHTML + "</a>";
                    console.log("    Key field is now link");
                } else {
                    keyField.setAttribute("style", "cursor: pointer;");
                    keyField.addEventListener("click", function(event) {
                        GM_setClipboard(inner, "{ type: 'text', mimetype: 'text/plain'}");
                    });
                    console.log("    Key field is now copiable");
                }
            }
        },

        addToMemory: function(ifVisited)
        {
            this.updateID();
            USER.VISITED_GA_LIST[this.ID] = ifVisited;
            USER.save();
            console.log("    Giveaway with ID " + this.ID + " added to visited GA list with status:", ifVisited);
        }
    };

    /*
        Raffle data
    */
    var RAFFLE =
    {
        ID: "",
        updateID: function()
        {
            this.ID = document.URL.match(/\?id=[0-9]{1,}/)[0].slice(4);
            console.log("    Updated raffle ID: ", this.ID);
            return this.ID;
        },

        TASKS_ARRAY: [], // List of tasks
        updateTaskArray: function()
        {
            if (this.TASKS_ARRAY.length) {
                return;
            }
            var taskElements = Array.prototype.slice.call(document.getElementsByClassName("panel-body")[1].children);
            for (var i = 0; i < taskElements.length; i++) {
                if (taskElements[i].innerHTML.indexOf("<a class=\"btn btn-danger btn-xs\">") == -1) {
                    taskElements.splice(i, 1);
                    i--;
                }
            }
            for (var i = 0; i < taskElements.length; i++) {
                var tID = taskElements[i].children[0].children[1].getAttribute("data-target").substr(9);
                this.TASKS_ARRAY[i] =
                {
                    "element": taskElements[i],
                    "id": tID,
                    "buttonsContainer": document.getElementById("collapse" + tID),
                    "inner": taskElements[i].innerText.substr(1, taskElements[i].innerText.length - 3),
                    "steam": false // null or URL of group
                };
                if (this.TASKS_ARRAY[i]["inner"].match(REGEXP.STEAM_TASK)) {
                    var str = this.TASKS_ARRAY[i]["buttonsContainer"].getElementsByTagName("a")[0].getAttribute("href");
                    str = str.split("/")[4].replace(/[#]/, ""); // Some unexcepted symbols
                    str = str.toLowerCase(); // lower case

                    this.TASKS_ARRAY[i]["steam"] =
                    {
                        id: str,
                        ifJoined: false
                    };
                }
            }
            console.log("    Updated task array: ", this.TASKS_ARRAY);
            return this.TASKS_ARRAY;
        },

        updateSteamTasksJoinStatus: function ()
        {
            for (var i = 0; i < this.TASKS_ARRAY.length; i++) {
                if (this.TASKS_ARRAY[i]["steam"]) {
                    if (findInArray(USER.GROUPS_ARRAY, this.TASKS_ARRAY[i]["steam"]["id"]) == -1) {
                        this.TASKS_ARRAY[i]["steam"]["ifJoined"] = false;
                    } else {
                        this.TASKS_ARRAY[i]["steam"]["ifJoined"] = true;
                    }
                }
            }
        },

        highlight: function()
        {
            console.log("    Highlighting tasks");
            this.updateTaskArray();
            for (var i = 0; i < this.TASKS_ARRAY.length; i++) {
                console.log("    Task #" + i);
                var tElem = this.TASKS_ARRAY[i]["element"].children[0].children[0];
                if (this.TASKS_ARRAY[i]["steam"]) {
                    console.log("    Group URL: " + this.TASKS_ARRAY[i]["steam"]["id"]);
                    if (findInArray(USER.GROUPS_ARRAY, this.TASKS_ARRAY[i]["steam"]["id"]) != -1) {
                        setBoxShadow(tElem, "g");
                    } else {
                        if (IF_LOADED_ACTUAL_DATA) {
                            setBoxShadow(tElem, "r");
                        } else {
                            setBoxShadow(tElem, "y");
                        }
                    }
                } else if (this.TASKS_ARRAY[i]["inner"].indexOf("Sign up and earn free Steam Wallet codes!") != -1) {
                    setBoxShadow(tElem, "p");
                } else if (this.TASKS_ARRAY[i]["inner"].indexOf("Twitch") != -1) {
                    setBoxShadow(tElem, "p");
                } else {
                    setBoxShadow(tElem, "b");
                    /*
                        Here must remove links and click;
                        !!!
                    */
                }
            }
            console.log("    Highlighted all tasks");
        },

        addToMemory: function(ifVisited)
        {
            this.updateID();
            USER.VISITED_RAFFLE_LIST[this.ID] = ifVisited;
            USER.save();
            console.log("    Giveaway with ID " + this.ID + " added to visited raffle list with status:", ifVisited);
        }
    };

    /*
        Dashboard / Raffle list data
    */
    var DASHBOARD =
    {
        type: "",
        getType: function()
        {
            if (document.URL.indexOf("dashboard.php") != -1) this.type = "G";
            if (document.URL.indexOf("raffles.php") != -1) this.type = "R";
            return this.type;
        },
        highlight: function()
        {
            var eventContainersArray = document.getElementsByClassName("panel-body")[1].getElementsByClassName("row");
            for (var i = 0; i < eventContainersArray.length; i++) {
                var id = eventContainersArray[i].innerHTML.match(/\.php\?id=[0-9]{1,}/)[0].slice(8);
                var trg = eventContainersArray[i].getElementsByClassName("btn")[0];
                this.getType();
                var ifVisited;
                if (this.type == "G") ifVisited = USER.VISITED_GA_LIST[id];
                if (this.type == "R") ifVisited = USER.VISITED_RAFFLE_LIST[id];
                if (ifVisited) {
                    setBoxShadow(trg, "g");
                } else {
                    if (IF_LOADED_ACTUAL_DATA) {
                        setBoxShadow(trg, "r");
                    } else {
                        setBoxShadow(trg, "y");
                    }
                }
            }
        }
    };

    /*
        Regular expressions
    */
    var REGEXP =
    {
        STEAM_TASK: /Join .* Steam Group/,
        STEAM_KEY: /[A-Z0-9]{5}-[A-Z0-9]{5}-[A-Z0-9]{5}/,
        URL: /(https?:\/\/)?([\da-z\.-]+)\.([a-z\.]{2,6})([\/\w \.-]*)*\/?/
    };

    /*
        Interface
    */
    var INTERFACE =
    {
        createMenu: function()
        {
            console.log("    Creating interface");
            /*
                Show/Hide btn
            */
            var menuElement = document.getElementById("navcol-1").children[0];
            var liElement = document.createElement("li");
            liElement.innerHTML = "<a id=\"ME_settingsBtn\" class=\"hvr-pulse\" style=\"color:#a0a0a0; user-select:none;\">Marvelous_extended</a>";
            menuElement.appendChild(liElement);
            document.getElementById("ME_settingsBtn").addEventListener("click", this.toggle);

            /*
                Main interface window
            */
            var interfaceElement = document.createElement("div");
            interfaceElement.setAttribute("id", "ME_interfaceContainer");
            interfaceElement.setAttribute("style", "display:none;");
            interfaceElement.innerHTML += "<a id = \"ME_resetVisitedGAListBtn\" class=\"ME_btn\">Reset visited giveaways list</a><br>";
            document.body.appendChild(interfaceElement);

            /*
                Events
            */
            document.getElementById("ME_resetVisitedGAListBtn").addEventListener("click", this.resetVisitedGAList);
        },

        toggle: function()
        {
            var interfaceElement = document.getElementById("ME_interfaceContainer");
            if (interfaceElement.style.display == "none") {
                interfaceElement.style.display = "";
            } else {
                interfaceElement.style.display = "none";
            }
        },

        createJoinButtons: function()
        {
            if (!IF_LOADED_ACTUAL_DATA) {
                return;
            }
            var event_;
            if (LOCATION.indexOf("G") != -1) event_ = GIVEAWAY;
            if (LOCATION.indexOf("R") != -1) event_ = RAFFLE;

            event_.updateSteamTasksJoinStatus();
            for (var i = 0; i < event_.TASKS_ARRAY.length; i++) {
                if (event_.TASKS_ARRAY[i]["steam"]) {
                    var taskElem = event_.TASKS_ARRAY[i]["element"];
                    var joinBtn = document.createElement("a");
                    if (event_.TASKS_ARRAY[i]["steam"]["ifJoined"]) {
                        joinBtn.innerHTML = "Leave";
                    } else {
                        joinBtn.innerHTML = "Join";
                    }
                    joinBtn.setAttribute("class", "ME_btn ME_btn-join");
                    joinBtn.setAttribute("id", "ME_join-" + event_.TASKS_ARRAY[i]["steam"]["id"]);
                    taskElem.appendChild(joinBtn);

                    /*
                        Event
                    */
                    joinBtn.addEventListener("click", this.onJoinBtnClick);
                }
            }
        },

        /*
            Interface actions
        */

        onJoinBtnClick: function()
        {
            var btn = this;
            var groupID = btn.getAttribute("id").slice(8);
            if (btn.innerHTML == "Join") {
                btn.innerHTML = "Loading";
                joinSteamGroup(groupID, USER.SESSION_ID, function() {
                    btn.innerHTML = "Leave";
                    setBoxShadow(btn.parentNode.children[0].children[0], "g");
                });
            } else if (btn.innerHTML == "Leave") {
                btn.innerHTML = "Loading";
                leaveSteamGroup(groupID, USER.SESSION_ID, USER.PROCESS_URL, function() {
                    btn.innerHTML = "Join";
                    setBoxShadow(btn.parentNode.children[0].children[0], "r");
                });
            }
        },

        resetVisitedGAList: function() // Reset list of visited GA
        {
            USER.VISITED_GA_LIST = [];
            GM_notification({
                text: "Reseted visited giveaways list",
                title: "Marvelous_extended",
                image: "https://i.imgur.com/HddFKWQ.png",
                highlight: false,
                timeout: 3000
            });
            console.log("    Reseted visited GA list");
        }

    };

    /*
        CSS
    */
    var BOX_SHADOW_STYLE =
    {
        GREEN: "box-shadow:0 0 20px 3px #138d0e;",
        YELLOW: "box-shadow:0 0 20px 3px #7c7239;",
        RED: "box-shadow:0 0 20px 3px #720000;",
        BLUE: "box-shadow:0 0 20px 3px #134d7c;",
        PURPLE: "box-shadow:0 0 20px 3px #80008a;",
    };

    GM_addStyle(".ME_box_g {" + BOX_SHADOW_STYLE.GREEN + "} .ME_box_y {" + BOX_SHADOW_STYLE.YELLOW + "} .ME_box_r {" + BOX_SHADOW_STYLE.RED + "} .ME_box_b {" + BOX_SHADOW_STYLE.BLUE + "} .ME_box_p {" + BOX_SHADOW_STYLE.PURPLE + "}");
    GM_addStyle("#ME_interfaceContainer {position: absolute; z-index: 100; left: 30%; top: 60px; width: 40%; background: #222222; padding: 10px; color: #ffffff; border: 1px solid #1c1c1c; border-bottom-width: 3px; border-radius: 3px;}");
    GM_addStyle(".ME_btn {padding: 9px 12px 7px 0px; margin-bottom: 10px; border-width: 0 1px 4px 1px; font-size: 12px; text-transform: uppercase; color: #ffffff; background-color: #ff4136; border-color: #ff291c; display: inline-block; font-weight: normal; text-align: center; vertical-align: middle; cursor: pointer; background-image: none; border: 1px solid transparent; white-space: nowrap; padding: 7px 12px; font-size: 14px; line-height: 1.42857143; border-radius: 4px; user-select: none;} .ME_btn:hover{transform: scale(1.05); color:#ffffff;} .ME_btn:active{background-color: #ff1103; box-shadow:0 0 15px 3px #111111;}");
    GM_addStyle(".ME_btn-join {position: absolute; top: 5px; left: 0px; width: 90px}");

    function setBoxShadow(element, colorChar) // Color must be a char: g y r b p
    {
        element.className = element.className.replace(/\s?ME_box_./, ""); // Deleting previous class
        element.className += " ME_box_" + colorChar;
    }

/*
    Utility functions
*/

    function getSteamGroupId64(groupID, callback)
    {
        GM_xmlhttpRequest({
            url : "http://steamcommunity.com/groups/" + groupID + "/memberslistxml/?xml=1",
            method : "GET",
            onload: function(response) {
                var groupId = response["response"].match(/<groupID64>\d{1,}<\/groupID64>/)[0].slice(11, -12);
                callback(groupId);
            }
        });
    }

    function joinSteamGroup(groupID, sessionID, callback)
    {
        var groupURL = "http://steamcommunity.com/groups/" + groupID;
    	GM_xmlhttpRequest({
            url: groupURL,
            method: "POST",
            headers: {"Content-Type": "application/x-www-form-urlencoded; charset=UTF-8"},
            data: "action=join&sessionID=" + sessionID,
            onload: function(response)
            {
                console.log("    Got Steam request response: ", response);
                callback();
            }
        });
    }

    function leaveSteamGroup(groupID, sessionID, processURL, callback)
    {
        getSteamGroupId64(groupID, function(groupID64) {
            GM_xmlhttpRequest({
                url: processURL,
                method: "POST",
                headers: { "Content-Type": "application/x-www-form-urlencoded; charset=UTF-8" },
                data: "action=leaveGroup&sessionID=" + sessionID + "&groupId=" + groupID64,
                onload: function(response)
                {
                    console.log("    Got Steam request response: ", response);
                    callback();
                }
            });
        });
	}

    function findInArray(array, value, arrayElementModification = function(str) {return str;})
    {
        for (var i = 0; i < array.length; i++) {
            if (arrayElementModification(array[i]) == value) {
                return i;
            }
        }
        return -1;
    }

    function mergeJSON(base, add)
    {
        var out = {};
        for (var i in base){
            out[i] = base[i];
        }
        for (var i in add){
            if (out.hasOwnProperty(i)) {
                if (typeof out[i] === "object" && typeof add[i] === "object") {
                    mergeJSON(out[i], add[i]);
                }
            } else {
                out[i] = add[i];
            }
        }
        return out;
    }
/*
    ----------------------------------------------------------------------------
    Main
    ----------------------------------------------------------------------------
*/

    console.group("Marvelous_Extended Onload Info");
    console.log("\n");
    console.log("    Marvelous Extended " + GM_info.script.version);
    console.log("    Location: " + LOCATION);

    INTERFACE.createMenu();

    /*
        Giveaway
    */
    if (LOCATION.indexOf("G") != -1) {
        GIVEAWAY.modifyKeyField();

        USER.load(function() {
            INTERFACE.createJoinButtons();
            GIVEAWAY.highlight();
            GIVEAWAY.addToMemory(GIVEAWAY.ifGotReward());
        });
        GIVEAWAY.highlight();
        console.log("    Giveaway: ", GIVEAWAY);
    }

    /*
        Raffle (Disabled)
    */
    /*if (LOCATION.indexOf("R") != -1) {
        USER.load(function() {
            INTERFACE.createJoinButtons();
            RAFFLE.highlight();
            RAFFLE.addToMemory(true);
        });
        RAFFLE.highlight();
        console.log("    Raffle: ", RAFFLE);
    }*/

    /*
        Dashboard
    */
    if (LOCATION.indexOf("D") != -1) {
        USER.load(function() {
            DASHBOARD.highlight();
        });
        DASHBOARD.highlight();
        console.log("    Dashboard: ", DASHBOARD);
    }

    console.log("\n");
    console.groupEnd();
})();
